var crypto = require('crypto');

//
// The Server-Sent events code is loosely based on https://github.com/remy/eventsource-h5d
// (c) 2012 Remy Sharp, http://remysharp.com, released under the MIT license.
//

module.exports = function(app, config) {
  var response_id = 0;
  var logger = app;

  // A hash of the currently valid response objects (hashed by response.response_id).
  var connections = Object.create(null);
  // A hash of the subscribers per channel (hashed by channel and then by response.response_id).
  var subscribers = Object.create(null);

  // A backlog of messages.
  var history = [];
  // A counter of messages ids.
  var lastMessageId = 0;

  app.get('/subscribe', function (request, response, next) {
    if (request.headers.accept == 'text/event-stream') {
      // Generate a unique identifier for this socket.
      response.response_id = response_id++;
      response.channels = [];

      response.writeHead(200, {
        'content-type': 'text/event-stream',
        'cache-control': 'no-cache',
        'connection': 'keep-alive'
      });

      // Support standard HTTP requests via the polyfill.
      if (request.headers['x-requested-with'] == 'XMLHttpRequest') {
        response.xhr = null;
      }

      var cookie_name = config.get('eventsource:cookie_name');
      var session_id = request.cookies[cookie_name] ? request.cookies[cookie_name] : '';

      // Extract the channels from the request.
      if (request.query['channels']) {
        for (var i in request.query['channels']) {
          var channel = request.query['channels'][i];
          // Extract the channel and validate the token.
          var channel_splitted = channel.split(':');
          if (channel_splitted.length != 2) {
            continue;
          }
          var channel_name = channel_splitted[0];
          var token = channel_splitted[1];

          var hmac = crypto.createHmac('sha256', session_id + config.get('eventsource:private_key'));
          hmac.update(channel_name);
          if (token != hmac.digest('hex')) {
            logger.debug('Invalid channel token for ' + channel + ' (silently ignored).');
            continue;
          }

          // Channel is valid, subscribe to it.
          if (!subscribers[channel_name]) {
            subscribers[channel_name] = Object.create(null);
          }
          subscribers[channel_name][response.response_id] = response;
          response.channels.push(channel_name);
        }
      }
      else {
        logger.debug('No channel specified in request.');
        var err = new Error('Not Acceptable');
        err.status = 406;
        next(err);
      }

      if (request.headers['last-event-id']) {
        var id = parseInt(request.headers['last-event-id']);
        for (var i = 0; i < history.length; i++) {
          if (history[i].id > id) {
            sendSSE(response, history[i].id, history[i].event, history[i].message);
          }
        }
      } else {
        // resets the ID
        response.write('id\n\n');
      }

      // Attach the connection.
      connections[response.response_id] = response;
      logger.debug('Opened connection ' + response.response_id + ' subscribed to channels ' + (response.channels.length ? response.channels.join(', ') : '(no channels)'));

      request.on('close', function () {
        logger.debug('Closed connection ' + response.response_id);
        delete connections[response.response_id];
        for (var i in response.channels) {
          var channel = response.channels[i];
          delete subscribers[channel][response.response_id];
        }
      });
    } else {
      logger.debug('Invalid Accept header: ' + request.headers.accept);
      var err = new Error('Not Acceptable');
      err.status = 406;
      next(err);
    }
  });

  app.post('/broadcast', function (request, response, next) {
    if (!request.body.channels || !request.body.message) {
      var err = new Error('Not Acceptable');
      err.status = 406;
      next(err);
    }

    for (var i in request.body.channels) {
      var channel = request.body.channels[i];
      broadcast(channel, request.body.message);
    }
    response.writeHead(200);
    response.write('Ok');
    response.end();
  });

  // Send pings over the wire to avoid timeouts in proxy servers.
  if (config.get('eventsource:ping_interval')) {
    setInterval(function() {
      logger.debug('Sending ping to ' + Object.keys(connections).length + ' channel(s)');
      for (var i in connections) {
        var connection = connections[i];
        connection.write(':\n');
        if (connection.hasOwnProperty('xhr')) {
          clearTimeout(connection.xhr);
          connection.xhr = setTimeout(function () {
            connection.end();
            removeConnection(connection);
          }, 250);
        }
      }
    }, config.get('eventsource:ping_interval'));
  }

  function broadcast(event, message) {
    message = JSON.stringify(message);
    ++lastMessageId;
    history.push({
      id: lastMessageId,
      event: event,
      message: message
    });

    if (subscribers[event]) {
      logger.debug('Broadcasting message ' + lastMessageId + ' to ' + Object.keys(subscribers[event]).length + ' subscriber(s)');
      for (var i in subscribers[event]) {
        var connection = subscribers[event][i];
        sendSSE(connection, lastMessageId, event, message);
      }
    }
  }

  function sendSSE(response, id, event, message) {
    var data = '';
    if (event) {
      data += 'event: ' + event + '\n';
    }

    if (id) {
      data += 'id: ' + id + '\n';
    } else {
      data += 'id\n';
    }

    if (message) {
      data += 'data: ' + message.split(/\n/).join('\ndata:') + '\n';
    }
    data += '\n';

    response.write(data);

    if (response.hasOwnProperty('xhr')) {
      clearTimeout(response.xhr);
      response.xhr = setTimeout(function () {
        response.end();
        removeConnection(response);
      }, 250);
    }
  }
}
