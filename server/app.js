var fs = require('fs'),
    express = require('express'),
    config = require('nconf'),
    logger = require('winston');

// Load the configuration from the command line, the environment and the config file.
config
  .argv()
  .env()
  .file({ file: 'config.json' })
  .defaults({
    'http': {
      'port': 8081
    },
    'eventsource': {
      'private_key': 'xxxxxxyyyyyy',
      'cookie_name': 'SESSxxxxxyyyyy',
      'ping_interval': 15000
    }
  });

// Create the server.
var app = express();
app.configure(function() {
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.set('case sensitive routes', true);
});
app.listen(config.get('http:port'));

// Create the logging engine.
logger.extend(app);
logger.cli();

// Add our routes.
require('./routes')(app, config);

logger.info("Listening on port " + config.get('http:port'));
