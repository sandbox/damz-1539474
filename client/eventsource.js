
(function ($) {
  function execute_commands(commands) {
    Drupal.freezeHeight();

    for (var i in commands) {
      var command = commands[i];
      if (command['command'] && Drupal.ajax.prototype.commands[command['command']]) {
        Drupal.ajax.prototype.commands[command['command']](this, command);
      }
    }

    Drupal.unfreezeHeight();
  }

  Drupal.behaviors.eventsource = {
    attach: function(context) {
      console.log('Test');
      var url = Drupal.settings.basePath + 'subscribe?';
      $('*[data-eventsource]').each(function() {
        url = url + 'channels[]=' + $(this).attr('data-eventsource');
      });

      var source = new EventSource(url);
      source.addEventListener('eventsource-test-led', function(e) {
        var commands = JSON.parse(e.data);
        execute_commands(commands);
      }, false);
    }
  }

})(jQuery);
